

# Bot
- Scrim bot for LoL

# Setup

All you need to do is do 
```
npm install 
```
on your terminal or shell, and look at config.jsonc, make sure to fill it out!

Make sure you do not change, add or remove anything in these json files, **invites.json**, **scrims.json** and **teams.json**. They are essential for the bot to function and work, if you change them the bot might malfunction and may need a hard restart ( Going back to 0, recreating the invites again as well as the channels again)
