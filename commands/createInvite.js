const { prefix, adminID } = require("../config.json");
const fs = require("fs");

module.exports = (client) => {
  client.on("ready", () => {
    fs.readFile("./invites.json", "utf-8", (err, data) => {
      let inviteobj = JSON.parse(data);
      inviteobj = {};
      client.guilds.cache.each((guild) => {
        guild.fetchInvites().then((guildInvites) => {
          guildInvites.each((guildInvite) => {
            inviteobj[guildInvite.code] = guildInvite.uses;
            fs.writeFileSync(
              "./invites.json",
              JSON.stringify(inviteobj, null, 4),
              (err, data) => {
                console.log("Fetched all invites successfully");
              }
            );
          });
        });
      });
    });
  });

  client.on("message", (message) => {
    let messageArray = message.content.split(" ");
    let command = messageArray[0];
    let args = messageArray.slice(1);

    if (command === `${prefix}createinvite`) {
      if (!message.member.roles.cache.find((role) => role.id === adminID))
        return message.channel.send("Insufficient permissions");
      if (!args[0]) return message.channel.send("Please specify a team name!");

      var team = message.content.replace("!createinvite ", "");
      team.replace("s", "");

      fs.readFile("./teams.json", "utf8", async (err, data) => {
        console.log(JSON.parse(data))
        if (data === undefined || typeof data != 'object') {
          data = {"teams": []}
        }
        if (data.teams.includes(team)) {
          if (data.teams.includes(team))
            message.channel.send("That team already exists!");
          return;
        }

        message.channel
          .createInvite({
            maxUses: 5,
            unique: true,
            maxAge: 0,
          })
          .then((invite) => {
            var invitecode = invite.code;
            fs.readFile("./scrims.json", "utf8", (err, data) => {
              var scrim = JSON.parse(data);
              scrim[team] = {
                code: invitecode,
                members: [],
                state: "",
                guestInvite: "",
              };
              data.teams.push(team);
              fs.writeFile(
                "./teams.json",
                JSON.stringify(data, null, 4),
                async (err) => {}
              );

              fs.readFile("./invites.json", "utf8", (err, data) => {
                let inviteobj = JSON.parse(data);

                inviteobj[invite.code] = invite.uses;
                fs.writeFile(
                  "./invites.json",
                  JSON.stringify(inviteobj, null, 4),
                  (err) => {
                    fs.writeFile(
                      "./scrims.json",
                      JSON.stringify(scrim, null, 4),
                      async (err) => {
                        message.channel.send(
                          "Invite created! Here is the invite link for team " +
                            team +
                            ": https://discord.gg/" +
                            invite.code
                        );

                        //TC = Practise History, draft theory, notes
                        //VC = Team Meeting, Start Scrim (5 members), Join active scrim
                        const role = await message.guild.roles.create({
                          data: {
                            name: team,
                            color: "DEFAULT",
                          },
                        });

                        const category = await message.guild.channels.create(
                          team,
                          {
                            type: "category",
                            permissionOverwrites: [
                              {
                                id: role.id,
                                allow: ["VIEW_CHANNEL", "SEND_MESSAGES"],
                              },
                              {
                                id: message.guild.id,
                                deny: ["VIEW_CHANNEL", "SEND_MESSAGES"],
                              },
                            ],
                          }
                        );
                        message.guild.channels.create("practice-history", {
                          type: "text",
                          parent: category,
                        });
                        message.guild.channels.create("draft-theory", {
                          type: "text",
                          parent: category,
                        });
                        message.guild.channels.create("notes", {
                          type: "text",
                          parent: category,
                        });
                        message.guild.channels.create("Team Meeting", {
                          type: "voice",
                          parent: category,
                        });
                        message.guild.channels.create("Start Scrim", {
                          type: "voice",
                          parent: category,
                          userLimit: 1,
                        });

                        if (err) {
                          return message.channel.send(
                            "Unable to get create an invite!"
                          );
                        }
                      }
                    );
                  }
                );
              });
            });
          });
        message.channel
          .createInvite({ unique: true, maxAge: 0 })
          .then((invite) => {
            fs.readFile("./scrims.json", "utf8", (err, data) => {
              let scrim = JSON.parse(data);
              scrim[team].guestInvite = invite.code;

              fs.readFile("./guestinvites.json", "utf8", (err, data) => {
                let inviteobj = JSON.parse(data);

                inviteobj[`${invite.code}-${team}`] = invite.uses;

                fs.writeFile(
                  "./scrims.json",
                  JSON.stringify(scrim),
                  async (err) => {
                    message.channel.send(
                      "Guest invite for " +
                        team +
                        ": https://discord.gg/" +
                        invite.code
                    );
                    fs.writeFile(
                      "guestInvites.json",
                      JSON.stringify(inviteobj, null, 4),
                      (err) => {}
                    );

                    if (err) {
                      return message.channel.send(
                        "Unable to get create an invite!"
                      );
                    }
                  }
                );
              });
            });
          });
      });
    }
  });
};
