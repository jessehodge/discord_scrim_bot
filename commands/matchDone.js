const { prefix } = require("../config.json");
module.exports = (client) => {
  client.on("message", async (message) => {
    let messageArray = message.content.split(" ");
    let command = messageArray[0];
    let args = messageArray.slice(1);
    //start
    if (command == `${prefix}done`) {
      var scrim0 = message.content;
      var scrim = scrim0.replace(`!done `, "");

      if (!message.member.roles.cache.find((role) => role.name === "Captain"))
        return message.channel.send(
          "Insufficient permissions. Must be a captain"
        );
      if (
        !message.guild.channels.cache.find(
          (c) => c.type == "category" && c.name == scrim
        )
      )
        return message.channel.send("Scrim not found! (Case Sensitive)");
      const category = message.guild.channels.cache.find(
        (c) => c.name == scrim
      );
      console.log(category);
      const children = category.children;

      await children.forEach((channel) => {
        channel.delete();
      });
      category.delete();

      message.channel.send(`The scrim ${scrim} has been deleted!`);
    }
  });
};
