const { APIkey, prefix } = require("../config.json");
const fetch = require("node-fetch");
module.exports = (client) => {
  client.on("message", async (message) => {
    let messageArray = message.content.split(" ");
    let command = messageArray[0];
    let args = messageArray.slice(1);
    //start
    if (command === `${prefix}registname`) {
      message.channel.send("Please type in your IGN.");
      const filter = (msg) => msg.author === message.author;
      const collector = message.channel.createMessageCollector(filter, {
        time: 1000 * 60,
      }); //60 seconds, change the 60 to change it into other seconds
      collector.on("collect", async (m) => {
        var msg = m.content;
        var newmsg = msg.replace(/\s/g, "%20");
        const link = `https://na1.api.riotgames.com/lol/summoner/v4/summoners/by-name/${newmsg}?api_key=${APIkey}`;
        const result = await fetch(link);
        const data = await result.json();

        if (!data.status) {
          const user = message.guild.member(message.author);
          const nickname = user.nickname || user.user.username;

          console.log(nickname);
          let firstindex = nickname.indexOf("[");
          let secondindex = nickname.indexOf("]");

          let position = nickname.slice(firstindex, secondindex + 1);
          user.setNickname(position.concat(" " + m.content));
          message.channel.send(
            `Your account have been successfully linked to ${m.content}.\n Account Link: https://na.op.gg/summoner/userName=${newmsg}`
          );
        } else {
          message.channel.send(
            `Player not found! If you are sure that you did not make a mistake in providing a correct name, please present to the developer with **error code ${data.status.status_code}**!`
          );
        }

        collector.stop();
      });
    }
  });
};
