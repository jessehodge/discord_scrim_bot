const { prefix, adminID } = require("../config.json");
const fs = require("fs");
module.exports = (client) => {
  client.on("message", (message) => {
    let messageArray = message.content.split(" ");
    let command = messageArray[0];
    let args = messageArray.slice(1);

    if (command === `${prefix}deleteteam`) {
      if (!message.member.roles.cache.find((role) => role.id === adminID))
        return message.channel.send("Insufficient permissions");

      try {
        var team = message.content.replace("!deleteteam ", "");

        var category = message.guild.channels.cache.find((c) => c.name == team);
        console.log(team);
        if (
          !message.guild.channels.cache.find((channel) => channel.name == team)
        )
          return message.channel.send(
            "Team does not exist. (Case sensitive) If your team is Bravo, then it may not be 'bravo'"
          );
        fs.readFile("./scrims.json", "utf8", (err, scrimData) => {
          var scrim = JSON.parse(scrimData);
          fs.readFile("./invites.json", "utf8", (err, invitesData) => {
            fs.readFile("./teams.json", "utf8", async (err, teamData) => {
              fs.readFile(
                "./guestInvites.json",
                "utf8",
                async (err, guestInvitesData) => {
                  var guestinv = JSON.parse(guestInvitesData);

                  var teamobj = JSON.parse(teamData);

                  var invites = JSON.parse(invitesData);
                  for (var key in scrim) {
                  }
                  for (const invite in invites) {
                    if (invite == scrim[key].code) {
                      await message.guild
                        .fetchInvites()
                        .then((guildInvites) => {
                          console.log("i am fetching invites");
                          guildInvites.each((guildInvite) => {
                            console.log("i am in looping of ivites");

                            if (guildInvite.code == scrim[key].code) {
                              try {
                                guildInvite
                                  .delete()
                                  .then(() => {
                                    console.log("Deleting main inv");
                                    delete invites[invite];
                                    message.channel.send(
                                      `${team} has been deleted!`
                                    );
                                  })
                                  .catch((err) => {
                                    message.channel.send(
                                      "Unable to delete the invite, or the invite doesn't exist!"
                                    );
                                  });
                              } catch (err) {
                                console.log(err);
                              }
                            }
                            if (guildInvite.code == scrim[key].guestInvite) {
                              try {
                                guildInvite.delete().catch((err) => {
                                  console.log("deleting guestinv");
                                  delete scrim[key];

                                  delete guestinv[invite];
                                  message.channel.send(
                                    "Unable to delete the guest invite, or the invite doesn't exist!"
                                  );
                                });
                              } catch (err) {
                                console.log(err);
                              }
                            }
                          });
                        });
                      if (
                        message.guild.channels.cache.find((c) => c.name == team)
                      ) {
                        const children = category.children;
                        await children.forEach((channel) => {
                          channel.delete();
                        });

                        category.delete();
                      }
                      let teamIndex = teamobj.teams.indexOf(team);
                      const newteamobj = teamobj.teams.splice(teamIndex, 1);
                      fs.writeFile(
                        "./scrims.json",
                        JSON.stringify(scrim, null, 4),
                        (err) => {}
                      );
                      fs.writeFile(
                        "./teams.json",
                        JSON.stringify(newteamobj, null, 4),
                        (err) => {}
                      );
                      fs.writeFile(
                        "./invites.json",
                        JSON.stringify(invites, null, 4),
                        (err) => {}
                      );
                      fs.writeFile(
                        "./guestInvites.json",
                        JSON.stringify(guestinv, null, 4),
                        (err) => {}
                      );
                    }
                  }
                }
              );
            });
          });
        });
      } catch (err) {
        return message.channel.send(
          "Something went wrong! Make sure that I have permissions."
        );
      }
    }
  });
};
