const Discord = require("discord.js");
const { prefix, adminID } = require("../config.json");
module.exports = (client) => {
  client.on("message", async (message) => {
    let messageArray = message.content.split(" ");
    let command = messageArray[0];
    let args = messageArray.slice(1);

    if (command === `${prefix}registreaction`) {
      if (message.member.roles.cache.some((role) => role.name === adminID))
        return message.channel.send("Insufficient permissions");
      const topEmoji = message.guild.emojis.cache.find(
        (emoji) => emoji.name === "Top"
      );
      const jungleEmoji = message.guild.emojis.cache.find(
        (emoji) => emoji.name === "Jungle"
      );
      const midEmoji = message.guild.emojis.cache.find(
        (emoji) => emoji.name === "Mid"
      );
      const botEmoji = message.guild.emojis.cache.find(
        (emoji) => emoji.name === "Bot"
      );
      const supportEmoji = message.guild.emojis.cache.find(
        (emoji) => emoji.name === "Support"
      );

      const embed = new Discord.MessageEmbed()
        .setTitle("Registration")
        .setDescription("Pick your roles by reacting!")
        .addFields({
          name: "Position",
          value: `
          ${topEmoji} Top\
          \n\n ${jungleEmoji} Jungle\
          \n\n ${midEmoji} Mid\
          \n\n ${botEmoji} ADC\
          \n\n ${supportEmoji} Support`,
        })
        .setTimestamp();
      const msg = await message.channel.send(embed);
      msg.react(topEmoji);
      msg.react(jungleEmoji);
      msg.react(midEmoji);
      msg.react(botEmoji);
      msg.react(supportEmoji);
    }
  });
};
