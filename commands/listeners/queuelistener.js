
const { APIkey } = require("../../config.json");
const fs = require("fs");
const fetch = require("node-fetch");
module.exports = (client) => {
  client.on("voiceStateUpdate", async (member, newstate) => {

    fs.readFile("./scrims.json", "utf8", async (err, data) => {
      var scrim = JSON.parse(data);
      fs.readFile("./teams.json", "utf8", async (err, teamsdata) => {
        var teams = JSON.parse(teamsdata);
        var channel = newstate.guild.channels.cache.get(newstate.channelID || member.channelID) ||
          member.guild.channels.cache.get(newstate.channelID || member.channelID);
        var channelParent =
          newstate.guild.channels.cache.get(channel.parentID) ||
          member.guild.channels.cache.get(channel.parentID);

        try {
          if (!teams.teams.includes(channelParent.name)) return;
        } catch (err) {
          return;
        }

        if (
          channel.full === false &&
          teams.teams.includes(channelParent.name)
        ) {
          for (var teamcurrent in scrim) {
            console.log(teamcurrent);

            if (scrim[teamcurrent].members.includes(newstate.id || member)) {
              scrim[teamcurrent].state = "idle";
              fs.writeFileSync(
                "./scrims.json",
                JSON.stringify(scrim, null, 4),
                (err) => {}
              );
            }
          }
        }

        if (channel.full === true && teams.teams.includes(channelParent.name)) {
          //change team state to queue
          for (var teamcurrent in scrim) {
            try {
              if (scrim[teamcurrent].members.includes(member.id)) {
                scrim[teamcurrent].state = "queue";
                fs.writeFileSync(
                  "./scrims.json",
                  JSON.stringify(scrim, null, 4),
                  (err) => {}
                );
              }
            } catch (err) {}
          }

          //check for other queing teams
          for (var teamoppose in scrim) {
            console.log("checking for other teams");
            if (
              !scrim[teamoppose].members.includes(newstate.id) &&
              scrim[teamoppose].state === "queue"
            ) {
              console.log(teamoppose, teamcurrent);
              console.log(scrim[teamoppose].members);
              console.log("2 teams in queue!");
              scrim[teamoppose].state = "idle";
              scrim[teamcurrent].state = "idle";
              fs.writeFile(
                "./scrims.json",
                JSON.stringify(scrim, null, 4),
                (err) => {}
              );
              scrim[teamoppose].members.forEach((player) => {
                let user =
                  newstate.guild.member(player) || member.guild.member(player);
                if (!user.roles.cache.find((r) => r.name === "Captain")) {
                  scrim[teamcurrent].members.forEach((player) => {
                    let user =
                      newstate.guild.member(player) ||
                      member.guild.member(player);
                    if (!user.roles.cache.find((r) => r.name === "Captain")) {
                      let role = user.guild.roles.cache.find((r) => r.name === "Captain")
                      user.roles.add(role);
                    }
                  });
                }
              });
              var teamcurrentRole = await newstate.guild.roles.cache.find(
                (r) => r.name == teamcurrent
              );

              var teamopposeRole = await newstate.guild.roles.cache.find(
                (r) => r.name == teamoppose
              );

              const category = await newstate.guild.channels.create(
                `${teamcurrent} VS ${teamoppose}`,
                {
                  type: "category",
                  permissionOverwrites: [
                    {
                      id: teamcurrentRole,
                      allow: ["VIEW_CHANNEL", "SEND_MESSAGES"],
                    },
                    {
                      id: member.guild.id,
                      allow: ["VIEW_CHANNEL"],
                      deny: ["SEND_MESSAGES", "CONNECT"],
                    },
                    {
                      id: teamopposeRole,
                      allow: ["VIEW_CHANNEL", "SEND_MESSAGES"],
                    },
                  ],
                }
              );

              const playerinfoChannel = await newstate.guild.channels.create(
                `player-information`,
                {
                  type: "text",
                  parent: category,
                  permissionOverwrites: [
                    {
                      id: teamcurrentRole,
                      allow: ["VIEW_CHANNEL", "SEND_MESSAGES"],
                    },
                    {
                      id: member.guild.id,
                      allow: ["VIEW_CHANNEL"],
                    },
                    {
                      id: teamopposeRole,
                      allow: ["VIEW_CHANNEL", "SEND_MESSAGES"],
                    },
                  ],
                }
              );
              const teamopposeChannel = await newstate.guild.channels.create(
                teamoppose,
                {
                  type: "voice",
                  parent: category,
                  userLimit: 10,
                  permissionOverwrites: [
                    {
                      id: member.guild.id,
                      allow: ["VIEW_CHANNEL"],
                    },
                    {
                      id: teamopposeRole,
                      allow: ["VIEW_CHANNEL", "CONNECT"],
                    },
                  ],
                }
              );

              const teamcurrentChannel = await newstate.guild.channels.create(
                teamcurrent,
                {
                  type: "voice",
                  parent: category,
                  userLimit: 10,
                  permissionOverwrites: [
                    {
                      id: member.guild.id,
                      allow: ["VIEW_CHANNEL"],
                    },
                    {
                      id: teamcurrentRole,
                      allow: ["VIEW_CHANNEL", "CONNECT"],
                    },
                  ],
                }
              );
              try {
                scrim[teamcurrent].members.forEach((member) => {
                  let player = newstate.guild.member(member);
                  player.voice.setChannel(teamcurrentChannel);
                });
                scrim[teamoppose].members.forEach((member) => {
                  let player = newstate.guild.member(member);
                  player.voice.setChannel(teamopposeChannel);
                });
              } catch (err) {
                console.log(
                  err +
                    "\n ignore this error, this was intended and the error is handled."
                );
              }
              const capRole = newstate.guild.roles.cache.find(
                (r) => r.name === "Captain"
              );
              await newstate.guild.channels.create("Team Captains", {
                type: "voice",
                parent: category,
                userLimit: 2,
                permissionOverwrites: [
                  {
                    id: member.guild.id,
                    allow: ["VIEW_CHANNEL"],
                  },
                  {
                    id: capRole,
                    allow: ["VIEW_CHANNEL", "CONNECT"],
                  },
                ],
              });
              fs.readFile("./activeScrims.json", "utf8", async (err, data) => {
                var activeScrims = JSON.parse(data);

                var teamCurrentStart = await newstate.guild.channels.create(
                  `Join when ready ${teamcurrent}`,
                  {
                    type: "voice",
                    parent: category,
                    userLimit: 10,
                    permissionOverwrites: [
                      {
                        id: member.guild.id,
                        allow: ["VIEW_CHANNEL"],
                      },
                      {
                        id: teamcurrentRole,
                        allow: ["VIEW_CHANNEL", "CONNECT"],
                      },
                    ],
                  }
                );
                var teamOpposeStart = await newstate.guild.channels.create(
                  `Join when ready ${teamoppose}`,
                  {
                    type: "voice",
                    parent: category,
                    userLimit: 10,
                    permissionOverwrites: [
                      {
                        id: member.guild.id,
                        allow: ["VIEW_CHANNEL"],
                      },
                      {
                        id: teamopposeRole,
                        allow: ["VIEW_CHANNEL", "CONNECT"],
                      },
                    ],
                  }
                );
                activeScrims[`${teamcurrent}-vs-${teamoppose}`] = {
                  teamCurrent: teamcurrent,
                  teamOppose: teamoppose,
                  teamCurrentStart: teamCurrentStart.id,
                  teamOpposeStart: teamOpposeStart.id,
                };
                fs.writeFile(
                  "./activeScrims.json",
                  JSON.stringify(activeScrims, null, 4),
                  (err) => {}
                );
              });

              var playersCurrent = [];
              scrim[teamcurrent].members.forEach((player) => {
                let user = member.guild.member(player);
                let nickname = user.nickname;
                console.log("i am in nickname");

                let firstindex = nickname.indexOf("[");
                let secondindex = nickname.indexOf("]");
                // let indexes = `${firstindex}` + `${secondindex}`

                let position = nickname.slice(firstindex + 1, secondindex);
                let newnick = nickname.slice(secondindex + 2);
                newnick = newnick.replace(/\s/g, "%20");
                playersCurrent.push(
                  `**${position}:** <@${player}> [Player Information](https://na.op.gg/summoner/userName=${newnick}) `
                );
              });

              var playersOppose = [];
              scrim[teamoppose].members.forEach((player) => {
                let user = member.guild.member(player);
                let nickname = user.nickname;

                let firstindex = nickname.indexOf("[");
                let secondindex = nickname.indexOf("]");
                // let indexes = `${firstindex}` + `${secondindex}`

                let position = nickname.slice(firstindex + 1, secondindex);
                let newnick = nickname.slice(secondindex + 2);
                newnick = newnick.replace(/\s/g, "%20");
                playersOppose.push(
                  `**${position}:** <@${player}> [Player Information](https://na.op.gg/summoner/userName=${newnick}) `
                );
              });

              await fetch(
                "https://www.passwordrandom.com/query?command=password"
              ).then((res) => {
                res.text().then((password) => {
                  const embed = new Discord.MessageEmbed()
                    .setTitle(`${teamcurrent} vs ${teamoppose}`)
                    .setDescription(
                      `Lobby Name: **${teamcurrent}-vs-${teamoppose}**\nPassword: **${password}**\n\n\n**${teamcurrent} Roster**\n${
                        playersCurrent.join("\n") || "No Players"
                      }\n **${teamoppose} Roster**\n${
                        playersOppose.join("\n") || "No Players"
                      }`
                    );
                  playerinfoChannel.send(embed);
                });
              });

              return;
            }
          }
        }
      });
    });

    async function startNextGame(
      teamOpposeStart,
      teamCurrentStart,
      teamcurrent,
      teamoppose,
      playersCurrent,
      playersOppose
    ) {
      await fetch("https://www.passwordrandom.com/query?command=password").then(
        (res) =>
          res.text().then((password) => {
            console.log("ue");
            const newembed = Discord.MessageEmbed()
              .setTitle(`${teamcurrent} vs ${teamoppose}`)
              .setDescription(
                `Lobby Name: ${teamcurrent}-vs-${teamoppose}\nPassword: ${password}\n\n\n**${teamcurrent} Roster**\n${
                  playersCurrent.join("\n") || "No Players"
                }\n **${teamoppose} Roster**\n${
                  playersOppose.join("\n") || "No Players"
                }`
              );
            msg.edit({ embeds: [newembed] });
            teamOpposeStart.setName(
              `Game Ready ${teamoppose}`
            );
            teamCurrentStart.setName(
              `Game Ready ${teamcurrent}`
            );
          })
      );

      scrim[teamcurrent].members.forEach((player) => {
        let user = member.guild.member(player);
        let nickname = user.nickname;

        let firstindex = nickname.indexOf("[");
        let secondindex = nickname.indexOf("]");
        // let indexes = `${firstindex}` + `${secondindex}`

        let position = nickname.slice(firstindex + 1, secondindex);
        let newnick = nickname.slice(secondindex + 2);
        newnick = newnick.replace(/\s/g, "%20");
        fetch(
          `https://na1.api.riotgames.com/lol/summoner/v4/summoners/by-name/${newnick}?api_key=${APIkey}`
        )
          .then((res) => res.json())
          .then((data) => {
            const puuid = data.puuid;
            fetch(
              `https://americas.api.riotgames.com/lol/match/v5/matches/by-puuid/${puuid}/ids?start=0&count=20&api_key=${APIkey}`
            )
              .then((res) => res.json())
              .then((data) => {
                const matchId = data[0];
                fetch(
                  `https://americas.api.riotgames.com/lol/match/v5/matches/${matchId}?api_key=${APIkey}`
                )
                  .then((res) => res.json())
                  .then((data) => {
                    scrim[teamcurrent].members.forEach((player) => {
                      let memberObj = member.guild.member(player);
                      if (
                        memberObj.nickname.includes(
                          data.participants[0].summonerName
                        )
                      ) {
                        var winner;
                        var loser;
                        var mapId = data.info.mapId;
                        if (data.participants[0].win == true) {
                          winner = teamcurrent;
                        } else {
                          loser = teamoppose;
                        }
                        //

                        const playerinformationCurrent = member.guild.channels.cache.find(
                          (c) => c.parent == teamcurrent
                        );
                        const playerinformationOppose = member.guild.channels.cache.find(
                          (c) => c.parent == teamoppose
                        );
                        fetch(
                          "https://static.developer.riotgames.com/docs/lol/maps.json"
                        )
                          .then((res) => res.json())
                          .then((data) => {
                            data.each((map) => {
                              if (map.mapId == mapId) {
                                var mapData = {
                                  mapName: map.mapName,
                                  mapNotes: map.notes,
                                };
                                const currentEmbed = new Discord.MessageEmbed()
                                  .setTitle(`Against ${teamoppose}`)
                                  .setDescription(
                                    `**Map:** ${mapData.mapName}/${mapData.mapNotes}\n`
                                  )
                                  .setTimestamp();
                                playerinformationCurrent.send(currentEmbed);
                                const opposeEmbed = new Discord.MessageEmbed()
                                  .setTitle(`Against ${teamcurrent}`)
                                  .setDescription(
                                    `**Map:** ${mapData.mapName}/${mapData.mapNotes}\n`
                                  )
                                  .setTimestamp();
                                playerinformationOppose.send(opposeEmbed);
                              }
                            });
                          });
                      }
                    });
                  });
              });
          });
      });
    }
  });
};
