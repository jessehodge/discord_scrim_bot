module.exports = (client) => {
  client.on("messageReactionAdd", (reaction, user) => {
    if (user.bot) return;
    if (reaction.emoji.name === "Top") {
      const member = reaction.message.guild.member(user);
      member.setNickname(`[Top] ${user.username}`);
    } else if (reaction.emoji.name === "Jungle") {
      const member = reaction.message.guild.member(user);
      member.setNickname(`[Jungle] ${user.username}`);
    } else if (reaction.emoji.name === "Mid") {
      const member = reaction.message.guild.member(user);
      member.setNickname(`[Mid] ${user.username}`);
    } else if (reaction.emoji.name === "Bot") {
      const member = reaction.message.guild.member(user);
      member.setNickname(`[ADC] ${user.username}`);
    } else if (reaction.emoji.name === "Support") {
      const member = reaction.message.guild.member(user);
      member.setNickname(`[Support] ${user.username}`);
    }
  });

  client.on("messageReactionRemove", (reaction, user) => {
    if (reaction.emoji.name === "Top") {
      const member = reaction.message.guild.member(user);
      member.setNickname(`${user.username}`);
    } else if (reaction.emoji.name === "Jungle") {
      const member = reaction.message.guild.member(user);
      member.setNickname(`${user.username}`);
    } else if (reaction.emoji.name === "Mid") {
      const member = reaction.message.guild.member(user);
      member.setNickname(`${user.username}`);
    } else if (reaction.emoji.name === "Bot") {
      const member = reaction.message.guild.member(user);
      member.setNickname(`${user.username}`);
    } else if (reaction.emoji.name === "Support") {
      const member = reaction.message.guild.member(user);
      member.setNickname(`${user.username}`);
    }
  });
};
