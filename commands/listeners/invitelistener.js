const fs = require("fs");
module.exports = async (client) => {
  client.on("guildMemberAdd", async (member) => {
    fs.readFile("./invites.json", "utf-8", (err, data) => {
      let inviteobj = JSON.parse(data);
      fs.readFile("./scrims.json", "utf-8", async (err, data) => {
        let scrimobj = JSON.parse(data);
        member.guild.fetchInvites().then((guildInvites) => {
          guildInvites.each((invite) => {
            for (const key in inviteobj) {
              if (invite.uses != inviteobj[invite.code]) {
                for (let key in scrimobj) {
                  if (invite.code === scrimobj[key].code) {
                    if (!scrimobj[key].members.includes(member.id)) {
                      scrimobj[key].members.push(member.id);
                      try {
                        if (scrimobj[key].members[0] == member.id) {
                          let role = member.guild.roles.cache.find((r) => r.name == "Captain")
                          member.roles.add(role);
                          let teamrole = member.guild.roles.cache.find(
                            (r) => r.name == key
                          );
                          member.roles.add(teamrole);
                        }
                      } catch (err) {
                        let role = member.guild.roles.create({
                          data: {
                            name: "Captain",
                          },
                        });
                        member.roles.add(role);
                      }
                    }

                    fs.writeFile(
                      "./scrims.json",
                      JSON.stringify(scrimobj, null, 4),
                      (err) => {}
                    );
                    inviteobj = JSON.parse(data);
                    inviteobj = {};
                    client.guilds.cache.each((guild) => {
                      guild.fetchInvites().then((guildInvites) => {
                        guildInvites.each((guildInvite) => {
                          inviteobj[guildInvite.code] = guildInvite.uses;

                          fs.writeFileSync(
                            "./invites.json",
                            JSON.stringify(inviteobj, null, 4),
                            (err, data) => {}
                          );
                        });
                      });
                    });
                  }
                }
              }
            }
          });
        });
      });
      fs.readFile("./guestInvites.json", "utf-8", async (err, guestData) => {
        fs.readFile("./scrims.json", "utf-8", async (err, scrimData) => {
          fs.readFile("./invites.json", "utf-8", async (err, inviteData) => {
            var scrimobj = JSON.parse(scrimData);

            let guestinv = JSON.parse(guestData);
            member.guild.fetchInvites().then((guildInvites) => {
              guildInvites.each((invite) => {
                for (const team in scrimobj) {
                  if (invite.uses != inviteobj[invite.code]) {
                    for (let key in guestinv) {
                      if (invite.code === guestinv[`${invite.code}-${team}`]) {
                        scrim[team].guestInvite = invite.code;
                        let role = member.guild.roles.cache.find(
                          (r) => r.name == team
                        );
                        member.roles.add(role);
                        setTimeout(() => {
                          member.kick();
                        }, 28800000);

                        fs.writeFile(
                          "./guestinvites.json",
                          JSON.stringify(guestinv, null, 4),
                          (err) => {}
                        );
                        inviteobj = JSON.parse(inviteData);
                        inviteobj = {};
                        client.guilds.cache.each((guild) => {
                          guild.fetchInvites().then((guildInvites) => {
                            guildInvites.each((guildInvite) => {
                              inviteobj[guildInvite.code] = guildInvite.uses;

                              fs.writeFileSync(
                                "./invites.json",
                                JSON.stringify(inviteobj, null, 4),
                                (err, data) => {}
                              );
                            });
                          });
                        });
                      }
                    }
                    break;
                  }
                }
              });
            });
          });
        });
      });
    });
  });
};
