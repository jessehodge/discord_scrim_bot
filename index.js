const Discord = require("discord.js");
const fs = require("fs");
const client = new Discord.Client();
const { prefix, token } = require("./config.json");

const createinvite = require("./commands/createInvite");
const registlistener = require("./commands/listeners/registlistener");
const registration = require("./commands/registration.js");

const registname = require("./commands/registname");
const invitelistener = require("./commands/listeners/invitelistener");
const queuelistener = require("./commands/listeners/queuelistener");
const deleteTeam = require("./commands/deleteTeam");
const matchDone = require("./commands/matchDone");


client.on("message", async (message) => {
  let messageArray = message.content.split(" ");
  let command = messageArray[0];
  let args = messageArray.slice(1);

  if (command === `${prefix}help`) {
    const embed = new Discord.MessageEmbed()
      .setTitle(`Help Page`)
      .setDescription(
        "!createinvite - ``!createinvite <teamname>``\
        \n\n!deleteteam - ``!deleteteam <teamname>``\
        \n\n!registname - ``!registname``\
        \n\n!registreaction - Brings up a reaction panel"
      )
      .setTimestamp();

    message.channel.send(embed);
  }
});

invitelistener(client);
createinvite(client);
registration(client);
registlistener(client);

registname(client);
queuelistener(client);
deleteTeam(client);
matchDone(client);
client.login(token);
